var Treeview = {
    init: function () {
        $("#m_tree_1").jstree({
            core: {
                themes: {
                    responsive: !1
                }
            },
            types: {
                default: {
                    icon: "fa fa-folder"
                },
                file: {
                    icon: "fa fa-file"
                }
            },
            plugins: ["types"]
        }), $("#m_tree_2").jstree({
            core: {
                themes: {
                    responsive: !1
                }
            },
            types: {
                default: {
                    icon: "fa fa-folder m--font-warning"
                },
                file: {
                    icon: "fa fa-file  m--font-warning"
                }
            },
            plugins: ["types"]
        }), $("#m_tree_2").on("select_node.jstree", function (e, t) {
            var n = $("#" + t.selected).find("a");
            if ("#" != n.attr("href") && "javascript:;" != n.attr("href") && "" != n.attr("href")) return "_blank" == n.attr("target") && (n.attr("href").target = "_blank"), document.location.href = n.attr("href"), !1
        }), $("#m_tree_3").jstree({
            plugins: ["wholerow", "checkbox", "types"],
            core: {
                themes: {
                    responsive: !1
                },
                data: [{
                    text: "Dự thảo luật kinh tế",
                    children: [{
                        text: "Dự thảo luật thuế sửa đổi",
                        state: {
                            selected: !0
                        }
                    }, {
                        text: "Luật đầu tư công",
                        icon: "fa fa-warning m--font-danger"
                    }, {
                        text: "Luật doanh nghiệp",
                        icon: "fa fa-folder m--font-default",
                        state: {
                            opened: !0
                        },
                        children: ["Dự thảo"]
                    }, {
                        text: "Luật Đơn vị hành chính kinh tế đặc biệt",
                        icon: "fa fa-warning m--font-waring"
                    }, {
                        text: "Luật thuế tài sản",
                        icon: "fa fa-check m--font-success",
                        state: {
                            disabled: !0
                        }
                    }]
                }, "Luật cạnh tranh sửa đổi"]
            },
            types: {
                default: {
                    icon: "fa fa-folder m--font-warning"
                },
                file: {
                    icon: "fa fa-file  m--font-warning"
                }
            }, plugins: ["contextmenu", "state", "types", "dnd"]
        }), $("#m_tree_4").jstree({
            core: {
                themes: {
                    responsive: !1
                },
                check_callback: !0,
                data: [{
                    text: "Báo cáo giám sát",
                    children: [{
                        text: "Giám sát cổ phần hóa DN nhà nước",
                        state: {
                            selected: !0
                        }
                    }, {
                        text: "Quản lý và sử dụng vốn",
                        icon: "fa fa-warning m--font-danger"
                    }, {
                        text: "Tài sản",
                        icon: "fa fa-folder m--font-success",
                        state: {
                            opened: !0
                        },
                        children: [{
                            text: "Giám sát tài sản công",
                            icon: "fa fa-file m--font-waring"
                        }]
                    }, {
                        text: "Báo cáo giám sát sử dụng vốn ngấn sách",
                        icon: "fa fa-warning m--font-waring"
                    }, {
                        text: "Giám sát chi ngân sách",
                        icon: "fa fa-check m--font-success",
                        state: {
                            disabled: !0
                        }
                    }, {
                        text: "Giám sát thu ngân sách",
                        icon: "fa fa-folder m--font-danger",
                        children: [{
                            text: "Năm 2018",
                            icon: "fa fa-file m--font-waring"
                        }, {
                            text: "Năm 2017",
                            icon: "fa fa-file m--font-success"
                        }, {
                            text: "Năm 2016",
                            icon: "fa fa-file m--font-default"
                        }, {
                            text: "Năm 2015",
                            icon: "fa fa-file m--font-danger"
                        }, {
                            text: "Năm 2014",
                            icon: "fa fa-file m--font-info"
                        }]
                    }]
                }, "Các báo cáo khác"]
            },
            types: {
                default: {
                    icon: "fa fa-folder m--font-brand"
                },
                file: {
                    icon: "fa fa-file  m--font-brand"
                }
            },
            state: {
                key: "demo2"
            },
            plugins: ["contextmenu", "state", "types"]
        }),


            $("#m_tree_demo").jstree({
                core: {
                    themes: {
                        responsive: !1
                    },
                    check_callback: !0,
                    data: [{
                        text: "Các dự thảo luật kinh tế",
                        children: [{
                            text: "Dự thảo luật doanh nghiệp sửa đổi",
                            state: {
                                selected: !0
                            }
                        }, {
                            text: "Dự thảo luật đầu tư công",
                            icon: "fa fa-warning m--font-danger"
                        }, {
                            text: "Dự thảo luật cạnh tranh",
                            icon: "fa fa-folder m--font-success",
                            state: {
                                opened: !0
                            },
                            children: [{
                                text: "Dự thảo luật cổ phần hóa",
                                icon: "fa fa-file m--font-waring"
                            }]
                        }, {
                            text: "Tài sản công",
                            icon: "fa fa-warning m--font-waring"
                        }, {
                            text: "Quản lý vốn",
                            icon: "fa fa-check m--font-success",
                            state: {
                                disabled: !0
                            }
                        }, {
                            text: "Dự thảo luật",
                            icon: "fa fa-folder m--font-danger",
                            children: [{
                                text: "Dự thảo mới",
                                icon: "fa fa-file m--font-waring"
                            }, {
                                text: "Dự thảo đã thông qua",
                                icon: "fa fa-file m--font-success"
                            }, {
                                text: "Dự thảo chưa thông qua",
                                icon: "fa fa-file m--font-default"
                            }, {
                                text: "Dự thảo cần theo dõi",
                                icon: "fa fa-file m--font-danger"
                            }, {
                                text: "Dự thảo cần bổ sung",
                                icon: "fa fa-file m--font-info"
                            }]
                        }]
                    }, "Các tài liệu khác liên quan dự thảo"]
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-brand"
                    },
                    file: {
                        icon: "fa fa-file  m--font-brand"
                    }
                },
                state: {
                    key: "demo2"
                },
                plugins: ["contextmenu", "state", "types", "dnd"]
            }),


            $("#m_tree_demo_1").jstree({
                core: {
                    themes: {
                        responsive: !1
                    },
                    check_callback: !0,
                    data: [{
                        text: "Hồ sơ Dự thảo Luật kinh tế",
                        children: [{
                            text: "Luật doanh nghiệp",
                            state: {
                                selected: !0
                            }
                        }, {
                            text: "Dự thảo luật đầu tư công",
                            icon: "fa fa-warning m--font-danger"
                        }, {
                            text: "Dự thảo luật cạnh tranh",
                            icon: "fa fa-folder m--font-success",
                            state: {
                                opened: !0
                            },
                            children: [{
                                text: "Dự thảo luật cổ phần hóa",
                                icon: "fa fa-file m--font-waring"
                            }]
                        }, {
                            text: "Tài sản công",
                            icon: "fa fa-warning m--font-waring"
                        }, {
                            text: "Quản lý vốn",
                            icon: "fa fa-check m--font-success",
                            state: {
                                disabled: !0
                            }
                        }, {
                            text: "Dự thảo luật",
                            icon: "fa fa-folder m--font-danger",
                            children: [{
                                text: "Dự thảo mới",
                                icon: "fa fa-file m--font-waring"
                            }, {
                                text: "Dự thảo đã thông qua",
                                icon: "fa fa-file m--font-success"
                            }, {
                                text: "Dự thảo chưa thông qua",
                                icon: "fa fa-file m--font-default"
                            }, {
                                text: "Dự thảo cần theo dõi",
                                icon: "fa fa-file m--font-danger"
                            }, {
                                text: "Dự thảo cần bổ sung",
                                icon: "fa fa-file m--font-info"
                            }]
                        }]
                    }, "Các tài liệu khác liên quan dự thảo"]
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-brand"
                    },
                    file: {
                        icon: "fa fa-file  m--font-brand"
                    }
                },
                state: {
                    key: "demo2"
                },
                plugins: ["contextmenu", "state", "types", "dnd"]
            }),


            $("#m_tree_demo_2").jstree({
                core: {
                    themes: {
                        responsive: !1
                    },
                    check_callback: !0,
                    data: [{
                        text: "Hồ sơ Dự thảo Luật kinh tế",
                        children: [{
                            text: "Luật doanh nghiệp",
                            state: {
                                selected: !0
                            }
                        }, {
                            text: "Dự thảo luật đầu tư công",
                            icon: "fa fa-warning m--font-danger"
                        }, {
                            text: "Dự thảo luật cạnh tranh",
                            icon: "fa fa-folder m--font-success",
                            state: {
                                opened: !0
                            },
                            children: [{
                                text: "Dự thảo luật cổ phần hóa",
                                icon: "fa fa-file m--font-waring"
                            }]
                        }, {
                            text: "Tài sản công",
                            icon: "fa fa-warning m--font-waring"
                        }, {
                            text: "Quản lý vốn",
                            icon: "fa fa-check m--font-success",
                            state: {
                                disabled: !0
                            }
                        }, {
                            text: "Dự thảo luật",
                            icon: "fa fa-folder m--font-danger",
                            children: [{
                                text: "Dự thảo mới",
                                icon: "fa fa-file m--font-waring"
                            }, {
                                text: "Dự thảo đã thông qua",
                                icon: "fa fa-file m--font-success"
                            }, {
                                text: "Dự thảo chưa thông qua",
                                icon: "fa fa-file m--font-default"
                            }, {
                                text: "Dự thảo cần theo dõi",
                                icon: "fa fa-file m--font-danger"
                            }, {
                                text: "Dự thảo cần bổ sung",
                                icon: "fa fa-file m--font-info"
                            }]
                        }]
                    }, "Các tài liệu khác liên quan dự thảo"]
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-brand"
                    },
                    file: {
                        icon: "fa fa-file  m--font-brand"
                    }
                },
                state: {
                    key: "demo2"
                },
                plugins: ["contextmenu", "state", "types", "dnd"]
            }),




            $("#m_tree_demo_3").jstree({
                core: {
                    themes: {
                        responsive: !1
                    },
                    check_callback: !0,
                    data: [{
                        text: "Hồ sơ Dự thảo Luật kinh tế",
                        children: [{
                            text: "Luật doanh nghiệp",
                            state: {
                                selected: !0
                            }
                        }, {
                            text: "Dự thảo luật đầu tư công",
                            icon: "fa fa-warning m--font-danger"
                        }, {
                            text: "Dự thảo luật cạnh tranh",
                            icon: "fa fa-folder m--font-success",
                            state: {
                                opened: !0
                            },
                            children: [{
                                text: "Dự thảo luật cổ phần hóa",
                                icon: "fa fa-file m--font-waring"
                            }]
                        }, {
                            text: "Tài sản công",
                            icon: "fa fa-warning m--font-waring"
                        }, {
                            text: "Quản lý vốn",
                            icon: "fa fa-check m--font-success",
                            state: {
                                disabled: !0
                            }
                        }, {
                            text: "Dự thảo luật",
                            icon: "fa fa-folder m--font-danger",
                            children: [{
                                text: "Dự thảo mới",
                                icon: "fa fa-file m--font-waring"
                            }, {
                                text: "Dự thảo đã thông qua",
                                icon: "fa fa-file m--font-success"
                            }, {
                                text: "Dự thảo chưa thông qua",
                                icon: "fa fa-file m--font-default"
                            }, {
                                text: "Dự thảo cần theo dõi",
                                icon: "fa fa-file m--font-danger"
                            }, {
                                text: "Dự thảo cần bổ sung",
                                icon: "fa fa-file m--font-info"
                            }]
                        }]
                    }, "Các tài liệu khác liên quan dự thảo"]
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-brand"
                    },
                    file: {
                        icon: "fa fa-file  m--font-brand"
                    }
                },
                state: {
                    key: "demo2"
                },
                plugins: ["contextmenu", "state", "types", "dnd"]
            }),


            $("#m_tree_demo_4").jstree({
                core: {
                    themes: {
                        responsive: !1
                    },
                    check_callback: !0,
                    data: [{
                        text: "Hồ sơ Dự thảo Luật kinh tế",
                        children: [{
                            text: "Luật doanh nghiệp",
                            state: {
                                selected: !0
                            }
                        }, {
                            text: "Dự thảo luật đầu tư công",
                            icon: "fa fa-warning m--font-danger"
                        }, {
                            text: "Dự thảo luật cạnh tranh",
                            icon: "fa fa-folder m--font-success",
                            state: {
                                opened: !0
                            },
                            children: [{
                                text: "Dự thảo luật cổ phần hóa",
                                icon: "fa fa-file m--font-waring"
                            }]
                        }, {
                            text: "Tài sản công",
                            icon: "fa fa-warning m--font-waring"
                        }, {
                            text: "Quản lý vốn",
                            icon: "fa fa-check m--font-success",
                            state: {
                                disabled: !0
                            }
                        }, {
                            text: "Dự thảo luật",
                            icon: "fa fa-folder m--font-danger",
                            children: [{
                                text: "Dự thảo mới",
                                icon: "fa fa-file m--font-waring"
                            }, {
                                text: "Dự thảo đã thông qua",
                                icon: "fa fa-file m--font-success"
                            }, {
                                text: "Dự thảo chưa thông qua",
                                icon: "fa fa-file m--font-default"
                            }, {
                                text: "Dự thảo cần theo dõi",
                                icon: "fa fa-file m--font-danger"
                            }, {
                                text: "Dự thảo cần bổ sung",
                                icon: "fa fa-file m--font-info"
                            }]
                        }]
                    }, "Các tài liệu khác liên quan dự thảo"]
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-brand"
                    },
                    file: {
                        icon: "fa fa-file  m--font-brand"
                    }
                },
                state: {
                    key: "demo2"
                },
                plugins: ["contextmenu", "state", "types", "dnd"]
            }),



            $("#m_tree_5").jstree({
                core: {
                    themes: {
                        responsive: !1
                    },
                    check_callback: !0,
                    data: [{
                        text: "Hồ sơ các vấn đề quan trọng",
                        children: [{
                            text: "Lấy ý kiến hoàn thiện luật cạnh tranh",
                            state: {
                                selected: !0
                            }
                        }, {
                            text: "",
                            icon: "fa fa-warning m--font-danger"
                        }, {
                            text: "Luật kinh doanh bất động sản",
                            icon: "fa fa-folder m--font-success",
                            state: {
                                opened: !0
                            },
                            children: [{
                                text: "Thẩm tra sơ bộ luật phát triển đô thị",
                                icon: "fa fa-file m--font-waring"
                            }]
                        }, {
                            text: "Một số điều luật các tổ chức tín dụng",
                            icon: "fa fa-warning m--font-waring"
                        }, {
                            text: "Luật đấu giá",
                            icon: "fa fa-check m--font-success",
                            state: {
                                disabled: !0
                            }
                        }, {
                            text: "Sub Nodes",
                            icon: "fa fa-folder m--font-danger",
                            children: [{
                                text: "Vấn đề quan trọng 1",
                                icon: "fa fa-file m--font-waring"
                            }, {
                                text: "Vấn đề quan trọng 2",
                                icon: "fa fa-file m--font-success"
                            }, {
                                text: "Vấn đề quan trọng 3",
                                icon: "fa fa-file m--font-default"
                            }, {
                                text: "Vấn đề quan trọng 4",
                                icon: "fa fa-file m--font-danger"
                            }, {
                                text: "Vấn đề quan trọng 5",
                                icon: "fa fa-file m--font-info"
                            }]
                        }]
                    }, "Tài liệu khác"]
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-success"
                    },
                    file: {
                        icon: "fa fa-file  m--font-success"
                    }
                },
                state: {
                    key: "demo2"
                },
                plugins: ["dnd", "state", "types"]
            }), $("#m_tree_6").jstree({
                core: {
                    themes: {
                        responsive: !1
                    },
                    check_callback: !0,
                    data: {
                        url: function (e) {
                            return "https://keenthemes.com/metronic/preview/inc/api/jstree/ajax_data.php"
                        },
                        data: function (e) {
                            return {
                                parent: e.id
                            }
                        }
                    }
                },
                types: {
                    default: {
                        icon: "fa fa-folder m--font-brand"
                    },
                    file: {
                        icon: "fa fa-file  m--font-brand"
                    }
                },
                state: {
                    key: "demo3"
                },
                plugins: ["dnd", "state", "types"]
            })
    }
};
jQuery(document).ready(function () {
    Treeview.init()
});